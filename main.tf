<<<<<<< HEAD
#Deploy Networking Resources

module "networking" {
  source   = "./networking"
  vpc_cidr = "10.123.0.0/16"
  #ipv6_cidr_block = "::/0"
  public_sn_count = 3
  private_sn_count = 4
  public_cidr = [for i in range(2,255,2) : cidrsubnet("10.123.0.0/16", 8 , i)]
  private_cidr = [for i in range(1,255,2) : cidrsubnet("10.123.0.0/16", 8 , i)]
=======
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  required_version = "~> 1.0"

  backend "s3" {
    bucket = "aws-lz-tf-demo1337"
    key    = "network/dev"
    region = "us-east-1"
    profile = "AMZ-TF-LZ"
  }
}

locals {
  common_tags = {
    Environment = var.environment
    Product     = var.product
  }
}

provider "aws" {
  region = var.aws_region

  default_tags {
    tags = local.common_tags
  }
}

module "vpc" {
  source = "./modules/vpc"

  cidr      = var.vpc_cidr
  flow_logs = var.enable_vpc_flow_logs

  common_tags = local.common_tags
}

module "igw" {
  source = "./modules/internet-gateway"

  vpc_id = module.vpc.id

  common_tags = local.common_tags
}

module "public_subnet" {
  for_each            = toset(var.aws_availability_zones)
  source              = "./modules/public-subnet"
  availability_zone   = each.key
  cidr_block          = var.public_subnet_cidrs[each.key]
  internet_gateway_id = module.igw.id

  vpc_id = module.vpc.id

  common_tags = local.common_tags
}

module "nat_gateway" {
  for_each          = toset(var.aws_availability_zones)
  source            = "./modules/nat-gateway"
  availability_zone = each.key
  eip_allocation_id = var.aws_elastic_ip_allocation_ids[index(var.aws_availability_zones, each.key)]
  public_subnet_id  = module.public_subnet[each.key].id

  common_tags = local.common_tags
}

module "private_subnet" {
  for_each          = toset(var.aws_availability_zones)
  source            = "./modules/private-subnet"
  availability_zone = each.key
  cidr_block        = var.private_subnet_cidrs[each.key]
  nat_gateway_id    = module.nat_gateway[each.key].id

  vpc_id = module.vpc.id

  common_tags = local.common_tags
>>>>>>> cfd5a82 (new commit)
}
